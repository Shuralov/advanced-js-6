let modalBtn = document.body.querySelector('.header__button');
let header = document.body.querySelector('.header');

modalBtn.addEventListener('click', () => {
    let modal = document.body.querySelector('.modal');
    let savePost = document.body.querySelector('.savePost');

    savePost.addEventListener('click', () => {
        let modalTitle = document.body.querySelector('#title');
        let modalPost = document.body.querySelector('#post');
        let modalName = document.body.querySelector('#name');
        let modalSurname = document.body.querySelector('#surname');
        let modalEmail = document.body.querySelector('#email');

        let card = new Card;
        card.create(modalTitle.value, modalPost.value, modalName.value, modalSurname.value, modalEmail.value, postId = 1);
        modal.remove();
    })
    modal.classList.remove('hide');
})

class Card {
    constructor() {
    }
    create(postText, postTitle, userName, userSurname, userEmail, postId) {

        this._card = document.createElement('div');
        header.after(this._card);

        let card = this._card;

        let _postTitle = document.createElement('p');
        _postTitle.innerText = `Title: ${postTitle}`;
        this._card.append(_postTitle);

        let _postText = document.createElement('p');
        _postText.innerText = `Post: ${postText}`
        this._card.append(_postText);

        let _userName = document.createElement('p');
        _userName.innerText = `Name: ${userName}`
        this._card.append(_userName);

        let _userSurname = document.createElement('p');
        _userSurname.innerText = `Surname: ${userSurname}`
        this._card.append(_userSurname);

        let _userEmail = document.createElement('p');
        _userEmail.innerText = `E-mail: ${userEmail}`
        this._card.append(_userEmail);

        this._editBtn = document.createElement('button');
        this._editBtn.innerText = `Edit post`;
        this._card.append(this._editBtn);
        this._editBtn.addEventListener('click', edit);

        this._clearBtn = document.createElement('button');
        this._clearBtn.innerText = `Delete post`;
        this._card.append(this._clearBtn);
        this._clearBtn.addEventListener('click', clear);

        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts`,
            method: 'post',
            data: {postText, postTitle, userName, userSurname, userEmail, postId}
        })

        function clear() {
            console.log('del');
            card.remove();
            $.ajax({
                url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
                method: 'delete',
                data: {postText, postTitle, userName, userSurname, userEmail, postId}
            })
        }

        function edit() {
            let _postTextEdit = document.createElement('input');
            _postTextEdit.value = postText;
            this.after(_postTextEdit);

            this._saveBtn = document.createElement('button');
            this._saveBtn.innerText = `Save`;
            _postTextEdit.after(this._saveBtn);
            this._saveBtn.addEventListener('click', save);

            function save() {
                $.ajax({
                    url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
                    method: 'put',
                    data: {postText, postTitle, userName, userSurname, userEmail, postId}
                })
                _postText.innerText = `Post: ${_postTextEdit.value} `;
                this.style.display = 'none';
                _postTextEdit.style.display = 'none';
                _postTextEdit.value = '';
                document.querySelector('.current').addEventListener('click', edit)
                this._editBtn.addEventListener('click', edit);
            }
            this.classList.add('current');
            this.removeEventListener('click', edit);
        }
    }
}


function getPosts() {
    return $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts',
        dataType: "json"
    });
}

function getUsers() {
    return $.ajax({
        url: 'https://jsonplaceholder.typicode.com/users',
        dataType: "json"
    });
}

let card = new Card();
$.when(getPosts(), getUsers()).done(function (posts, users) {

    for (let i = 0; i < posts[0].length; i++) {
        let postText = posts[0][i].body;
        let postTitle = posts[0][i].title;
        let postId = posts[0][i].id;
        let user = posts[0][i].userId;

        for (let j = 0; j < users[0].length; j++) {
            if (users[0][j].id === user) {
                let userName = users[0][j].name;
                let userSurname = users[0][j].username;
                let userEmail = users[0][j].email;
                card.create(postText, postTitle, userName, userSurname, userEmail, postId);
            }
        }
    }
});



